import numpy as np
from scipy.io.netcdf import netcdf_file
import matplotlib.pyplot as plt

nc = netcdf_file('timeseries.nc','r')

tsurf = np.squeeze(nc.variables['tsurf_ts'][:])
tsurf_mar = np.squeeze(nc.variables['tsurf_mar_ts'][:])
smb = np.squeeze(nc.variables['smb_ts'][:])*86.4e6
smb_mar = np.squeeze(nc.variables['smb_mar_ts'][:])*86.4e6
print tsurf.shape

ntime = len(tsurf[:,0])
time = np.arange(ntime)/(ntime/12.)

plt.figure(figsize=(6,6))

plt.subplot(211)
plt.axhline(273.15,c='k')
plt.plot(time,tsurf[:,0],lw=2,label='SEMIC (land)',alpha=0.5)
plt.plot(time,tsurf_mar[:,0],lw=2,label='MAR/ERA-40 (land)',alpha=0.5)
plt.plot(time,tsurf[:,1],lw=2,label='SEMIC (ice)',alpha=0.5)
plt.plot(time,tsurf_mar[:,1],lw=2,label='MAR/ERA-40 (ice)',alpha=0.5)
plt.xlabel('time (months)')
plt.ylabel('surface temperature (K)')
plt.grid()
plt.legend(framealpha=0.5,prop={'size':10})
plt.subplot(212)
plt.axhline(0,c='k')
plt.plot(time,smb[:,0],lw=2,label='SEMIC (land)',alpha=0.5)
plt.plot(time,smb_mar[:,0],lw=2,label='MAR/ERA-40 (land)',alpha=0.5)
plt.plot(time,smb[:,1],lw=2,label='SEMIC (ice)',alpha=0.5)
plt.plot(time,smb_mar[:,1],lw=2,label='MAR/ERA-40 (ice)',alpha=0.5)
plt.xlabel('time (months)')
plt.ylabel('surface mass balance (mm/day)')
plt.grid()
plt.legend(framealpha=0.5,prop={'size':10})

plt.tight_layout()
plt.show()


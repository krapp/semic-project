program main_program

    use surface_driver

    implicit none
    integer :: y, d
    character(len=256) :: arg
    type(smb_class) :: smb1

    ! read namelist filename
    call getarg(1, arg)
    smb1%nml_filename = trim(arg)

    total_time = 0.0d0

    call smb_init(smb1,"test",0)
    call smb_init_forcing(smb1)
    call smb_overwrite_init(smb1)
    if (smb1%driver%multi) then
        smb1%driver%multi = .false.
        do y = 1, smb1%nloop - 1

            write(*,*) 'loop:', y

            ! Daily time step
            do d = 1, smb1%driver%ntime

                ! Calculate SMB 
                call smb_update(smb1,d,y)

            end do
        end do
        smb1%driver%multi = .true.
        smb1%t = 0
        smb1%tnow = 0.0
        smb1%driver%day_of_year = 1
        smb1%driver%year = smb1%driver%year - 1
    end if

    do y = 1, smb1%nloop

        if (smb1%driver%multi) call smb_init_forcing(smb1)

        write(*,*) 'loop:', y

        ! Daily time step
        do d = 1, smb1%driver%ntime

            ! Calculate SMB 
            call smb_update(smb1,d,y)

        end do
        if (smb1%driver%multi) then
            call smb_output(smb1,y)
        else
            if (y.eq.smb1%nloop) call smb_output(smb1,1)
       end if
    end do


    call smb_end(smb1)
    write(*,*) 'total time for surface_physics:', smb1%nloop, total_time

end program main_program

program optimize

!    use mpi
    use smb
    use utils

    implicit none
    integer :: y, d, i, i0, i1
    integer :: ierr, num_procs, my_id
    type(smb_class)   :: smb1
    character(len=256) :: nml_file, arg, prefix, output
    logical :: file_exists
    real :: start, finish

    double precision, dimension(2) :: cost_stt, cost_smb, cost_swnet, cost_melt
    double precision :: cost


    ! read number of startfile
    call getarg(1,arg)
    read(arg,*) prefix
    call getarg(2,arg)
    read(arg,*) i0
    call getarg(3,arg)
    read(arg,*) i1

!    call MPI_INIT ( ierr )
!    !     find out MY process ID, and how many processes were started.
!
!    call MPI_COMM_RANK (MPI_COMM_WORLD, my_id, ierr)
!    call MPI_COMM_SIZE (MPI_COMM_WORLD, num_procs, ierr)


!    do i=0,num_procs-1
!        if (i == my_id) call sleep(my_id*10)
!    end do
    call smb_init(smb1,"test",0)
    call smb_init_forcing(smb1)

    ! read through all name lists in ~/data/tmp
    do i = i0,i1
        call cpu_time(start)
        write(nml_file,"(A,I3.3,A)") trim(prefix), i, ".nml"
        inquire(file=nml_file, exist=file_exists)
        if (.not. file_exists) then
            write(*,*) "file '", trim(nml_file), "' not found, exiting loop"
            ! STOP PROGRAM if file list ends (is there a better way?)
            !stop 0
            exit
        end if
        write(*,*) "\x1B[32msmb_optimize:\x1B[0m read file:", trim(nml_file)
        call smb_reset(smb1,nml_file)
        ! open file for CRMSD output
        write(output,"(A,I3.3,A)") trim(prefix), i,  ".out"
        open(2,file=trim(output),form='formatted')
        write(*,"(A,I2.2,A)") "loop ",smb1%nloop, " times over forcing fields"
        do y = 1, smb1%nloop

            ! Daily time step
            do d = 1, smb1%driver%ntime

                ! Calculate SMB 
                call smb_update(smb1,d,y)

            end do
        end do
        cost_stt    = calculate_cost(smb1%state%tsurf,smb1%vali%tsurf,smb1%basin,smb1%surface%par%nx,smb1%driver%ntime)
        cost_melt   = calculate_cost(smb1%state%melt,smb1%vali%melt,smb1%basin,smb1%surface%par%nx,smb1%driver%ntime)
        cost_smb    = calculate_cost(smb1%state%massbal,smb1%vali%massbal,smb1%basin,smb1%surface%par%nx,smb1%driver%ntime)
        cost_swnet  = calculate_cost(smb1%state%swnet,smb1%vali%swnet,smb1%basin,smb1%surface%par%nx,smb1%driver%ntime)
        cost = (cost_swnet(2)+cost_stt(2)+cost_smb(2))/3.0
        !write(2,*) cost_stt, cost_melt, cost_smb, cost_swnet
        write(2,*) cost
        close(2)
        call cpu_time(finish)
        print '("Time = ",f6.3," seconds.")',finish-start
    end do

    call smb_end(smb1)

!    call MPI_FINALIZE ( ierr )

end program optimize 

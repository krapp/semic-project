!> ####################################################################
!! **Module**  : surface_driver \n
!! **Author**  : Mario Krapp \n 
!! **Purpose** : This module contains all functionality and subroutines 
!!               to initialize and drive the subroutiones of the 
!!               surface_physics module
!! ####################################################################
module surface_driver

    use ncio 
    use coordinates
    use utils
    use surface_driver_utilities
    use surface_physics

    implicit none 

    double precision, parameter :: hsnow_init = 1.0d0

    double precision :: total_time !< global variable to keep track of total elapsed time (for benchmarking)

    type files_class !< Store names of output files
        character(len=256) :: timser !< file for time series
        character(len=256) :: daily !< file for daily output
    end type

    type driver_class !< Store values for various cases
        integer :: year        !< actual year
        integer :: ntime       !< current time step
        integer :: day_of_year !< actual day of the year
        logical :: multi       !< flag for input of multiple files
    end type

    type forc_class !< Stores all external forcing fields
        double precision, allocatable, dimension(:,:) :: tt   !< air temperature
        double precision, allocatable, dimension(:,:) :: rhoa !< air density
        double precision, allocatable, dimension(:,:) :: qq   !< specific humidity
        double precision, allocatable, dimension(:,:) :: sf   !< snow fall
        double precision, allocatable, dimension(:,:) :: rf   !< rain fall
        double precision, allocatable, dimension(:,:) :: lwd  !< downward LW
        double precision, allocatable, dimension(:,:) :: swd  !< downward SW
        double precision, allocatable, dimension(:,:) :: wind !< wind speed
        double precision, allocatable, dimension(:,:) :: sp   !< surface pressure
        double precision, allocatable, dimension(:,:) :: amp  !< diurnal cycle amplitude
    end type

    type vali_class !< Stores external validation fields
        double precision, allocatable, dimension(:,:) :: tsurf   !< surface temperature
        double precision, allocatable, dimension(:,:) :: alb     !< surface albedo
        double precision, allocatable, dimension(:,:) :: hsnow   !< snow height
        double precision, allocatable, dimension(:,:) :: melt    !< melt
        double precision, allocatable, dimension(:,:) :: refr    !< refreezing
        double precision, allocatable, dimension(:,:) :: massbal !< mass balance
        double precision, allocatable, dimension(:,:) :: subl    !< sublimation/evaporation
        double precision, allocatable, dimension(:,:) :: acc     !< accumulation
        double precision, allocatable, dimension(:,:) :: shf     !< sensible heat flux
        double precision, allocatable, dimension(:,:) :: lhf     !< latent heat flux
        double precision, allocatable, dimension(:,:) :: swnet   !< net SW
    end type            

    type state_class !< Stores calculated fields
        double precision, allocatable, dimension(:,:) :: tsurf   !< surface temperature
        double precision, allocatable, dimension(:,:) :: alb     !< surface albedo
        double precision, allocatable, dimension(:,:) :: hsnow   !< snow height
        double precision, allocatable, dimension(:,:) :: hice    !< ice thickness
        double precision, allocatable, dimension(:,:) :: melt    !< melt
        double precision, allocatable, dimension(:,:) :: refr    !< refreezing
        double precision, allocatable, dimension(:,:) :: massbal !< mass balance
        double precision, allocatable, dimension(:,:) :: subl    !< sublimation/evaporation
        double precision, allocatable, dimension(:,:) :: acc     !< accumulation
        double precision, allocatable, dimension(:,:) :: shf     !< sensible heat flux
        double precision, allocatable, dimension(:,:) :: lhf     !< latent heat flux
        double precision, allocatable, dimension(:,:) :: swnet   !< net SW
    end type

    type smb_class !< Container for all information needed to model a given domain (eg Greenland or Antarctica)

        type(surface_physics_class) :: surface    !< surface class (from module surface_physics: contains: parameter, forcing, and daily class
        type(grid_class)        :: grid   !< Grid definition (from module coordinates)

        character(len=256) :: grid_name !< Name of used grid

        character(len=256) :: nml_filename !< Name of namelist file
        character(len=256) :: surface_physics_nml !< Name of surface physics namelist file

        character(len=256) :: date_string !< Date string, e.g., "days since 01-01-2006"
        
        integer :: nsamples
        integer, allocatable, dimension(:) :: idx

        ! Static (per year or greater) variables
        integer, allocatable, dimension(:)   :: basin !< Store enumerated basins (0..N)
        integer, allocatable, dimension(:)   :: mask  !< 2D mask  
        integer :: t !< current time step
        integer :: nloop !< number of loops 
        double precision :: tnow !< current time
        logical :: read_forcing !< flag to check whether the forcing file needs to be read

        type(vali_class) :: vali !< validation object
        type(state_class) :: state !< state object
        type(forc_class) :: forc !< forcing object
        type(files_class) :: files !< output files object
        type(driver_class) :: driver !< driver object

    end type

    private
    public :: smb_init, smb_overwrite_init, smb_reset, smb_update, smb_output, &
              smb_end, smb_class, smb_init_forcing, total_time

contains

    !> Initialize surface and populate declared variables, derived types, etc.
    !!
    subroutine smb_init(dom,domname,year)

        implicit none

        integer, intent(in) :: year !< current year
        character(len=*), intent(in) :: domname !< domain name
        type(smb_class), intent(in out) :: dom !< domain object

        integer :: nx, ny, nloop, bdim, starty, nsamples
        character(len=256) :: file_timser, file_daily, &
            surface_physics_nml, &
            basin_file, grid_name, land_ice_ocean_file, &
            forc_file, vali_file

        logical :: multi
        
        ! variables for random sub-sampling
        integer :: i, k, xsize
        double precision, dimension(:), allocatable :: r
        integer, dimension(:), allocatable :: rand_i
        logical, allocatable, dimension(:) :: mask

        !> 1. Read namelist \n
        ! Declaration of name list parameters
        namelist /semic_output/ file_timser, file_daily
        ! namelist with settings for forcing 
        namelist /semic_driver/ surface_physics_nml, forc_file, vali_file, multi, starty, &
                               basin_file, land_ice_ocean_file, nloop, grid_name, nsamples
        open(7,file=dom%nml_filename)
        read(7,nml=semic_output)
        read(7,nml=semic_driver)
        close(7)

        dom%surface_physics_nml = surface_physics_nml

        !> 2. Initialize time stepping counter
        dom%tnow = 0.0
        dom%t = 0

        dom%nloop = nloop

        ! read forcing
        dom%read_forcing = .true.
        dom%driver%multi = multi
        dom%driver%day_of_year = 0

        dom%driver%year = -999
        dom%driver%ntime = -999

        !> 3. Assign file names to output files (files_class)
        dom%files%timser  = file_timser
        dom%files%daily   = file_daily

        !> 4. define grid name and initialize 2D grid
        dom%grid_name = grid_name

        write(*,"(A)") "\x1B[32msemic_init:\x1B[0m Create grid information."
        !  only if 2D output is requested
        select case(dom%grid_name)
        case("MAR")
            if (len_trim(dom%files%daily) > 0) then
                call grid_init(dom%grid,name="MAR-25KM",mtype="stereographic",units="kilometers",lon180=.true., &
                    x0=-775.d0,dx=25.d0,nx=60,y0=-1200.d0,dy=25.d0,ny=112, &
                    lambda=-40.d0,phi=70.5d0,alpha=7.5d0)
            else
                dom%grid%G%nx = 60
                dom%grid%G%ny = 112
            end if
        case("MAR-SUB")
            dom%grid%G%nx = 934
            dom%grid%G%ny = 1
        case("LOVECLIM")
            !call grid_init(dom%grid,name="LOVECLIM-T21",mtype="latlon",units="degrees",lon180=.true., &
            !    x0=0.d0,dx=5.625d0,nx=64,y0=-85.75d0,dy=5.5d0,ny=32)
            if (len_trim(dom%files%daily) > 0) then
                call grid_init(dom%grid,name="IcIES",mtype="latlon",units="degrees",lon180=.false., &
                    x0=0.0d0,dx=1.0d0,nx=360,y0=30.5d0,dy=1.0d0,ny=59)
            else
                dom%grid%G%nx = 360
                dom%grid%G%ny = 59
            end if
        case("ASR")
            if (len_trim(dom%files%daily) > 0) then
                call grid_init(dom%grid,name="ASR-30km",mtype="latlon",units="degrees",lon180=.false., &
                    x0=0.0d0,dx=1.0d0,nx=360,y0=30.0d0,dy=1.d0/6.d0,ny=360)
            else
                dom%grid%G%nx = 360
                dom%grid%G%ny = 360
            end if
        end select

        nx = dom%grid%G%nx
        ny = dom%grid%G%ny


        !> 5. Allocation of variables (surface_physics::surface_alloc)
        allocate(dom%mask(nx*ny))
        allocate(dom%basin(nx*ny))

        !> 6. Read ocean-land-ice (surface_physics::surface_state_class::mask, surface_driver::read_land_mask) or basin mask (smb_class::basin, surface_driver::read_basin_mask)
        if (len(trim(basin_file)).ne.0) then
            if (basin_file.eq."same") basin_file = forc_file
            ! read basin fields from file
            call read_basin_mask(basin_file,nx,ny,dom%basin)
            bdim = maxval(dom%basin)
            write(*,"(2A)") "\x1B[32msemic_init:\x1B[0m basin variable loaded from ", trim(basin_file)
        else
            bdim = 1
            dom%basin = 1
            write(*,"(A)") "\x1B[32msemic_init:\x1B[0m basin variable not loaded. Try land/ice/ocean mask."
        end if

        if (len(trim(land_ice_ocean_file)).ne.0) then
            if (land_ice_ocean_file.eq."same") land_ice_ocean_file = forc_file
            ! read land/ice/ocean fields from file
            call read_land_mask(land_ice_ocean_file,nx,ny,dom%mask)
            write(*,"(2A)") "\x1B[32msemic_init:\x1B[0m land/ice/ocean mask variable loaded from ", trim(land_ice_ocean_file)
            if (len(trim(basin_file)).eq.0) then
                call read_land_mask(land_ice_ocean_file,nx,ny,dom%basin)
                bdim = maxval(dom%basin)
                write(*,"(A)") "\x1B[32msemic_init:\x1B[0m Load land/ice/ocean as basin mask."
            end if

        else
            dom%mask = 2
            write(*,"(A)") "\x1B[32msemic_init:\x1B[0m land/ice/ocean mask not loaded, use 2 instead (valid only over ice)."
        end if
        if (len(trim(land_ice_ocean_file)).eq.0 .and. len(trim(basin_file)).eq.0) then
                bdim = 1
                dom%basin = 1
                write(*,"(A)") "\x1B[32msemic_init:\x1B[0m No basin mask found. Use whole domain instead."
        end if


        !> 7. Random sub-sampling of indices (optional); depends on namelist param 'nsamples'
        allocate(mask(nx*ny))
        dom%nsamples = nsamples
        mask = .false.
        where(dom%basin>0) mask = .true.
        xsize = int(sum(dom%basin*0.0+1.0,mask=mask))
        k = 1
        allocate(dom%idx(xsize))
        do i=1,size(dom%basin)
            if (mask(i)) then
                dom%idx(k) = i
                k = k + 1
            end if
        end do
        if ((dom%nsamples .gt. 0) .and. (dom%nsamples .le. xsize)) then
            allocate(r(dom%nsamples))
            call init_random_seed()
            call random_number(r)
            rand_i = dom%idx(int(r*xsize) + 1)
            deallocate(dom%idx)
            allocate(dom%idx(dom%nsamples))
            dom%idx = rand_i
            write(*,"(A,I4,A)") "\x1B[32msemic_init:\x1B[0m Random sub-sampling enabled (n = ", dom%nsamples, ")."
        end if
        do i=1,int(maxval(dom%basin))
            mask = .false.
            where(dom%basin.eq.i) mask = .true.
            if (int(maxval(dom%basin)) < 50) then
                write(*,"(A7,I4,A9,I5)")  "basin ", i, "has size", size(pack(dom%basin(dom%idx),mask(dom%idx)))
            end if
        end do

        !> 8. Allocate and initialize variables for surface_physics
        dom%surface%par%name     = trim(domname)
        dom%surface%par%nx       = size(dom%idx)

        call surface_alloc(dom%surface%now,size(dom%idx))
        dom%surface%now%hsnow = hsnow_init
        dom%surface%now%hice  = 0.0
        dom%surface%now%alb = 0.8
        dom%surface%now%tsurf = 260.0
        dom%surface%now%alb_snow = 0.8

        write(*,"(A)") "\x1B[32msemic_init:\x1B[0m allocated and initialized model variables."
        dom%surface%now%mask = dom%mask(dom%idx) 

        ! create correct date string
        write(dom%date_string,"(A,I4.4)") "days since 01-01-", starty

        !> 9. Initialize output files (files_class)
        if (len_trim(dom%files%timser) > 0) then
                call surface_write_ts_init(dom%files%timser,dom%t,dom%date_string,bdim)
        end if
        if (len_trim(dom%files%daily) > 0) then
                call surface_write_init(dom,file_daily,dom%t,dom%date_string)
        end if

        !> 9. Load namelist parameters into surface_physics::surface_param_class
        write(*,"(A)") "\x1B[32msemic_init:\x1B[0m Load parameters."
        call surface_physics_par_load(dom%surface%par,dom%surface_physics_nml)
        !call print_param(dom%surface%par)


    end subroutine smb_init

    !> Reset prognistic variables and reload model parameters
    subroutine smb_reset(dom,fnm,surface_physics_nml)

        type(smb_class), intent(in out) :: dom !< domain object
        character(len=256), intent(in) :: fnm, surface_physics_nml !< file name of namelist
        character(len=256) :: file_timser, file_daily

        namelist /semic_output/ file_timser, file_daily
        open(8,file=fnm)
        read(8,nml=semic_output)
        close(8)

        !> 1. Load namelist parameters into surface_physics::surface_param_class
        call surface_physics_par_load(dom%surface%par,surface_physics_nml)

        !> 2. Initialize time stepping counter
        dom%tnow = 0.0
        dom%t = 0

        !> 3. Assign file names to output files (files_class)
        dom%read_forcing = .true.
        dom%driver%multi = .false.
        dom%driver%day_of_year = 0

        !> 4. Initialise output files
        dom%files%timser  = file_timser
        dom%files%daily   = file_daily

        !> 5. Initialize output files (files_class)
        if (len_trim(dom%files%timser) > 0) then
                call surface_write_ts_init(dom%files%timser,dom%t,dom%date_string,maxval(dom%basin))
        end if
        if (len_trim(dom%files%daily) > 0) then
                call surface_write_init(dom,file_daily,dom%t,dom%date_string)
        end if

        !> 6. Re-initialize prognostic state variables (surface_physics::surface_state_class)
        dom%surface%now%hsnow = hsnow_init
        dom%surface%now%hice  = 0.0
        !dom%surface%now%alb = 0.8
        !dom%surface%now%tsurf = 260.
        !dom%surface%now%alb_snow = 0.8
        dom%surface%now%alb = dom%vali%alb(dom%idx,1)
        dom%surface%now%tsurf = dom%vali%tsurf(dom%idx,1)
        dom%surface%now%alb_snow = dom%vali%alb(dom%idx,1)
        !dom%surface%now%hsnow = dom%vali%hsnow(:,1)
        !where ((maxval(dom%vali%hsnow,2) - minval(dom%vali%hsnow,2) < 1.e-40) .and. (dom%surface%now%mask .eq. 2))
        !    dom%surface%now%hsnow = 5.0
        !elsewhere
        !    dom%surface%now%hsnow = dom%vali%hsnow(:,1)
        !end where

        write(*,"(2A)") "\x1B[32msemic_reset:\x1B[0m Read new namelist file: ", trim(surface_physics_nml)
        !call print_param(dom%surface%par)

    end subroutine smb_reset

    subroutine smb_overwrite_init(dom)
        type(smb_class), intent(in out) :: dom !< domain object

        dom%surface%now%alb      = dom%vali%alb(dom%idx,1)
        dom%surface%now%tsurf    = dom%vali%tsurf(dom%idx,1)
        dom%surface%now%alb_snow = dom%vali%alb(dom%idx,1)
        !dom%surface%now%hsnow = dom%vali%hsnow(:,1)
        dom%surface%now%hsnow    = hsnow_init
        !where ((maxval(dom%vali%hsnow,2) - minval(dom%vali%hsnow,2) < 1.e-40) .and. (dom%surface%now%mask .eq. 2))
        !    dom%surface%now%hsnow = 5.0
        !elsewhere
        !    dom%surface%now%hsnow = dom%vali%hsnow(:,1)
        !end where

        write(*,"(A)") "\x1B[32msemic_init:\x1B[0m Overwrite initial values."

    end subroutine smb_overwrite_init

    !> Initialize forcing.
    !> Two cases of forcing are possible (depending on namelist setting):
    !! 1. Looping over one forcing file, e.g., for model tuning, parameter tests
    !! 2. Forcing over a period of time (in years) with multiple input files
    !! 
    subroutine smb_init_forcing(dom)

        type(smb_class), intent(in out) :: dom !< domain object
        integer :: nx, ny, nt, nt_orig, starty, nloop, nsamples
        ! Declaration of forcing variables
        double precision, allocatable, dimension(:,:) :: sf, rf, sp, lwd, swd,&
            uu, vv, tt, qq, amp
        ! Declaration of validation variables
        double precision, allocatable, dimension(:,:) :: tsurf,alb,melt,&
            refr,massbal,subl,shf,lhf,hsnow
        ! Files, ...
        character(len=256) :: surface_physics_nml, &
                forc_file, vali_file, buffer, grid_name, &
                basin_file, land_ice_ocean_file
        ! switches for multi-year forcing (multi) and for file inquiries (exist)
        logical :: multi, exist

        !> 1. Read namelist with settings for forcing 
        namelist /semic_driver/ surface_physics_nml, grid_name, basin_file, land_ice_ocean_file, &
                               forc_file, vali_file, multi, starty, nloop, nsamples


        !> 2. Check if reading of additional forcing files is necessary
        if (dom%read_forcing) then

            open(7,file=dom%nml_filename)
            read(7,nml=semic_driver)
            close(7)

            dom%surface_physics_nml = surface_physics_nml

            dom%nsamples = nsamples
            
            ! make multi(-year forcing) available to module
            dom%driver%multi = multi

            ! set current year of forcing
            if (dom%driver%year.eq.-999) then
                dom%driver%year = starty
            else
                dom%driver%year = dom%driver%year + 1
            end if

            ! for multiple forcing files keep on reading
            if (dom%driver%multi .and. dom%read_forcing) then
                dom%read_forcing = .true.
                ! Inquire file name (format has to be <prefix><year>.nc)
                write(buffer,"(A,I4.4,A)") trim(forc_file), dom%driver%year, ".nc"
                inquire(file=buffer, exist=exist)
                if (.not. exist) then
                    write(*,"(3A)") "file '", trim(buffer), "' not found, exiting loop"
                    dom%read_forcing = .false.
                    ! STOP PROGRAM if file list ends (is there a better way?)
                    stop 0
                end if
                ! use current file as forcing file
                forc_file = buffer
            else
                dom%read_forcing = .false.
            end if


            dom%driver%ntime = -999
            nx = dom%grid%G%nx
            ny = dom%grid%G%ny

            if (len(trim(dom%grid_name)).ne.0) then
                ! Inquire number of time steps (sets main loop over days)
                if (dom%grid_name .eq. "ASR") then
                    nt = nc_size(forc_file,"Time")
                else
                    nt = nc_size(forc_file,"TIME")
                end if
                if (nt == 12) then ! in case of monthly data
                    nt_orig = nt
                    nt = 360
                end if
                dom%driver%ntime = nt

                !> 3. Allocate forcing variables
                allocate(tt(nx*ny,nt))
                allocate(sf(nx*ny,nt))
                allocate(rf(nx*ny,nt))
                allocate(sp(nx*ny,nt))
                allocate(lwd(nx*ny,nt))
                allocate(swd(nx*ny,nt))
                allocate(uu(nx*ny,nt))
                allocate(vv(nx*ny,nt))
                allocate(qq(nx*ny,nt))
                allocate(amp(nx*ny,nt))
                if (.not. allocated(dom%forc%tt))   allocate(dom%forc%tt(nx*ny,nt))
                if (.not. allocated(dom%forc%sf))   allocate(dom%forc%sf(nx*ny,nt))
                if (.not. allocated(dom%forc%rf))   allocate(dom%forc%rf(nx*ny,nt))
                if (.not. allocated(dom%forc%sp))   allocate(dom%forc%sp(nx*ny,nt))
                if (.not. allocated(dom%forc%lwd))  allocate(dom%forc%lwd(nx*ny,nt))
                if (.not. allocated(dom%forc%swd))  allocate(dom%forc%swd(nx*ny,nt))
                if (.not. allocated(dom%forc%wind)) allocate(dom%forc%wind(nx*ny,nt))
                if (.not. allocated(dom%forc%qq))   allocate(dom%forc%qq(nx*ny,nt))
                if (.not. allocated(dom%forc%rhoa)) allocate(dom%forc%rhoa(nx*ny,nt))
                if (.not. allocated(dom%forc%amp)) allocate(dom%forc%amp(nx*ny,nt))
               
                !> 4. Select forcing depending on the grid name defined in the namelist. Options are:
                !!     * MAR - data from the regional climate model _MAR_ (surface_driver::read_forcing_mar)
                !!     * LOVECLIM - data from the general circulation model _LOVECLIM_ (surface_driver::read_forcing_loveclim)
                !!     * ASR - data from the _Arctic System Re-analysis_ project (surface_driver::read_forcing_asr)
                select case (dom%grid_name)
                case ("MAR")
                    !!! MAR FORCING !!!
                    ! read forcing from file
                    write(*,"(2A)") "\x1B[32msemic_init_forcing:\x1B[0m Read MAR forcing file ", trim(forc_file)
                    call read_forcing_mar(forc_file,nx,ny,nt,uu,vv,tt,sf,rf,qq,sp,lwd,swd,amp)

                    ! load forcing files into 'forc' object, convert to SI units
                    dom%forc%sf   = sf/86.4e6 ! mm/day -> m/s
                    dom%forc%rf   = rf/86.4e6 ! mm/day -> m/s
                    dom%forc%sp   = sp*1.e2   ! hPa -> Pa
                    dom%forc%lwd  = lwd
                    dom%forc%swd  = swd
                    dom%forc%wind = uu*0.0
                    dom%forc%wind = sqrt(uu**2+vv**2)
                    dom%forc%rhoa = sp*1.e2/(287.*(tt+t0))
                    dom%forc%tt   = tt+t0 ! degC -> K
                    dom%forc%qq   = qq/1000.  ! g/kg -> kg/kg
                    dom%forc%amp  = amp
                    write(*,"(A,3I6)") "\x1B[32msemic_init_forcing:\x1B[0m MAR forcing variables loaded.", nx, ny, nt
                case ("MAR-SUB")
                    !!! MAR sub-sample FORCING !!!
                    ! read forcing from file
                    write(*,"(2A)") "\x1B[32msemic_init_forcing:\x1B[0m Read MAR-SUB forcing file ", trim(forc_file)
                    call read_forcing_mar_sub_sampling(forc_file,nx,nt,uu,vv,tt,sf,rf,qq,sp,lwd,swd,amp)

                    ! load forcing files into 'forc' object, convert to SI units
                    dom%forc%sf   = sf/86.4e6 ! mm/day -> m/s
                    dom%forc%rf   = rf/86.4e6 ! mm/day -> m/s
                    dom%forc%sp   = sp*1.e2   ! hPa -> Pa
                    dom%forc%lwd  = lwd
                    dom%forc%swd  = swd
                    dom%forc%wind = uu*0.0
                    dom%forc%wind = sqrt(uu**2+vv**2)
                    dom%forc%rhoa = sp*1.e2/(287.*(tt+t0))
                    dom%forc%tt   = tt+t0 ! degC -> K
                    dom%forc%qq   = qq/1000.  ! g/kg -> kg/kg
                    dom%forc%amp  = amp
                    write(*,"(A,3I6)") "\x1B[32msemic_init_forcing:\x1B[0m MAR-SUB forcing variables loaded.", nx, nt
                case ("LOVECLIM")
                    write(*,*) "\x1B[32msemic_init_forcing:\x1B[0m Read LOVECLIM forcing file ", trim(forc_file)
                    call read_forcing_loveclim(forc_file,nx,ny,nt_orig,nt,uu,tt,sf,rf,qq,sp,lwd,swd)
                    dom%forc%sf   = sf/(360.*86.4e5) ! cm/year -> m/s
                    dom%forc%rf   = (rf-sf)/(360.*86.4e5) ! precip = total precip - snow fall; cm/year -> m/s
                    dom%forc%sp   = sp
                    dom%forc%lwd  = lwd
                    dom%forc%swd  = swd
                    dom%forc%wind = uu
                    dom%forc%rhoa = sp/(287.*tt)
                    dom%forc%tt   = tt
                    dom%forc%qq   = qq*0.662*611.2*exp(17.67*(tt-t0)/(tt-29.66))/sp
                    !dom%forc%qq   = qq
                    write(*,*) "\x1B[32msemic_init_forcing:\x1B[0m LOVECLIM forcing variables loaded.", nx, ny, nt
                    ! this is just for testing
                    !do i=1,12
                    !    dom%tnow = dom%tnow + 1.d0
                    !    dom%t = i
                    !    dom%now%tsurf = dom%forc%swd(:,i)
                    !    call surface_write(dom,dom%files%daily)
                    !end do
                    !stop ! don't forget to remove that stop in operational mode
                    ! end testing
                case ("ASR")
                    write(*,*) "\x1B[32msemic_init_forcing:\x1B[0m Read ASR forcing file ", trim(forc_file)
                    call read_forcing_asr(forc_file,nx,ny,nt,uu,vv,tt,sf,rf,qq,sp,lwd,swd)
                    dom%forc%sf   = sf*rf/10.8e6
                    dom%forc%rf   = rf*(1.0-sf)/10.8e6 ! mm/3h -> m/s
                    dom%forc%sp   = sp
                    dom%forc%lwd  = lwd 
                    dom%forc%swd  = swd 
                    dom%forc%wind = uu*0.0
                    dom%forc%wind = sqrt(uu**2+vv**2)
                    dom%forc%rhoa = sp/(287.*tt)
                    dom%forc%tt   = tt 
                    dom%forc%qq   = qq 
                    write(*,*) "\x1B[32msemic_init_forcing:\x1B[0m ASR forcing variables loaded.", nx, ny, nt
                end select
            end if

            !> 5. If validation file is present in namelist, read it (similar to forcing)
            if (len(trim(vali_file)).ne.0) then
                if (vali_file.eq."same") vali_file = forc_file
                ! Allocate validation variables

                allocate(tsurf(nx*ny,nt))
                allocate(alb(nx*ny,nt))
                allocate(melt(nx*ny,nt))
                allocate(refr(nx*ny,nt))
                allocate(massbal(nx*ny,nt))
                allocate(subl(nx*ny,nt))
                allocate(lhf(nx*ny,nt))
                allocate(shf(nx*ny,nt))
                allocate(hsnow(nx*ny,nt))
                if (.not. allocated(dom%vali%tsurf))   allocate(dom%vali%tsurf(nx*ny,nt))
                if (.not. allocated(dom%vali%alb))     allocate(dom%vali%alb(nx*ny,nt))
                if (.not. allocated(dom%vali%melt))    allocate(dom%vali%melt(nx*ny,nt))
                if (.not. allocated(dom%vali%refr))    allocate(dom%vali%refr(nx*ny,nt))
                if (.not. allocated(dom%vali%massbal)) allocate(dom%vali%massbal(nx*ny,nt))
                if (.not. allocated(dom%vali%subl))    allocate(dom%vali%subl(nx*ny,nt))
                if (.not. allocated(dom%vali%lhf))     allocate(dom%vali%lhf(nx*ny,nt))
                if (.not. allocated(dom%vali%shf))     allocate(dom%vali%shf(nx*ny,nt))
                if (.not. allocated(dom%vali%acc))     allocate(dom%vali%acc(nx*ny,nt))
                if (.not. allocated(dom%vali%swnet))   allocate(dom%vali%swnet(nx*ny,nt))
                if (.not. allocated(dom%vali%hsnow))   allocate(dom%vali%hsnow(nx*ny,nt))

                if (.not. allocated(dom%state%tsurf))   allocate(dom%state%tsurf(nx*ny,nt))
                if (.not. allocated(dom%state%alb))     allocate(dom%state%alb(nx*ny,nt))
                if (.not. allocated(dom%state%melt))    allocate(dom%state%melt(nx*ny,nt))
                if (.not. allocated(dom%state%refr))    allocate(dom%state%refr(nx*ny,nt))
                if (.not. allocated(dom%state%massbal)) allocate(dom%state%massbal(nx*ny,nt))
                if (.not. allocated(dom%state%lhf))     allocate(dom%state%lhf(nx*ny,nt))
                if (.not. allocated(dom%state%shf))     allocate(dom%state%shf(nx*ny,nt))
                if (.not. allocated(dom%state%acc))     allocate(dom%state%acc(nx*ny,nt))
                if (.not. allocated(dom%state%swnet))   allocate(dom%state%swnet(nx*ny,nt))
                if (.not. allocated(dom%state%hsnow))   allocate(dom%state%hsnow(nx*ny,nt))
                if (.not. allocated(dom%state%hice))    allocate(dom%state%hice(nx*ny,nt))
                if (.not. allocated(dom%state%subl))    allocate(dom%state%subl(nx*ny,nt))

                select case (dom%grid_name)
                case ("MAR")
                    !!! MAR validation file
                    ! read validation fields from file
                    write(*,"(2A)") "\x1B[32msemic_init_forcing:\x1B[0m Read MAR validation file ", trim(vali_file)
                    call read_validation_mar(vali_file,nx,ny,nt,tsurf,alb,melt,refr,massbal,shf,lhf,hsnow)

                    ! load validation fields into 'vali' object, convert to SI units
                    dom%vali%tsurf   = tsurf + t0
                    dom%vali%alb     = alb
                    dom%vali%melt    = melt/86.4e6
                    dom%vali%refr    = refr/86.4e6
                    dom%vali%acc     = (massbal+melt-refr)/86.4e6
                    dom%vali%massbal = massbal/86.4e6
                    dom%vali%lhf     = lhf
                    dom%vali%shf     = shf
                    dom%vali%swnet   = (1.0-alb)*dom%forc%swd
                    dom%vali%hsnow   = hsnow
                    write(*,"(A)") "\x1B[32msemic_init_forcing:\x1B[0m MAR validation variables loaded."
                case ("MAR-SUB")
                    !!! MAR validation file
                    ! read validation fields from file
                    write(*,"(2A)") "\x1B[32msemic_init_forcing:\x1B[0m Read MAR-SUB validation file ", trim(vali_file)
                    call read_validation_mar_sub_sampling(vali_file,nx,nt,tsurf,alb,melt,refr,massbal,shf,lhf,hsnow)

                    ! load validation fields into 'vali' object, convert to SI units
                    dom%vali%tsurf   = tsurf + t0
                    dom%vali%alb     = alb
                    dom%vali%melt    = melt/86.4e6
                    dom%vali%refr    = refr/86.4e6
                    dom%vali%acc     = (massbal+melt-refr)/86.4e6
                    dom%vali%massbal = massbal/86.4e6
                    dom%vali%lhf     = lhf
                    dom%vali%shf     = shf
                    dom%vali%swnet   = (1.0-alb)*dom%forc%swd
                    dom%vali%hsnow   = hsnow
                    write(*,"(A)") "\x1B[32msemic_init_forcing:\x1B[0m MAR-SUB validation variables loaded."
                case ("LOVECLIM")
                    write(*,*) "\x1B[32msemic_init_forcing:\x1B[0m Read LOVECLIM validation file ", trim(vali_file)
                    call read_validation_loveclim(vali_file,nx,ny,nt_orig,nt,tsurf,alb)
                    dom%vali%tsurf   = tsurf + t0
                    dom%vali%alb     = alb
                    write(*,*) "\x1B[32msemic_init_forcing:\x1B[0m LOVECLIM validation variables loaded."
                end select
            end if
        
        end if

    end subroutine smb_init_forcing


    !> Update time steps of surface scheme
    subroutine smb_update(dom,day,year)

        type(smb_class), intent(in out) :: dom !< domain object
        integer, intent(in) :: day  !< current day
        integer, intent(in) :: year !< current year

        double precision :: start, finish

        !> 0. If nsamples .ne. -1 assumes to only take nsamples from total field

        !> 1. Assign forcing variables of current time step (day) to surface state object surface_physics::surface_state_class
        dom%surface%now%t2m  = dom%forc%tt(dom%idx,day)
        dom%surface%now%swd  = dom%forc%swd(dom%idx,day)
        dom%surface%now%lwd  = dom%forc%lwd(dom%idx,day)
        dom%surface%now%wind = dom%forc%wind(dom%idx,day)
        dom%surface%now%qq   = dom%forc%qq(dom%idx,day)
        dom%surface%now%sp   = dom%forc%sp(dom%idx,day)
        dom%surface%now%rhoa = dom%forc%rhoa(dom%idx,day)
        dom%surface%now%sf   = dom%forc%sf(dom%idx,day)
        dom%surface%now%rf   = dom%forc%rf(dom%idx,day)
        dom%surface%now%amp  = dom%forc%amp(dom%idx,day)
        !> 2. Check for prescribed surface variables (surface_physicsdom%idxdom%idxboundary_opt_class)
        if (dom%surface%bnd%tsurf) dom%surface%now%tsurf = dom%vali%tsurf(dom%idx,day)
        if (dom%surface%bnd%alb)   dom%surface%now%alb   = dom%vali%alb(dom%idx,day)
        if (dom%surface%bnd%melt)  dom%surface%now%melt  = dom%vali%melt(dom%idx,day)
        if (dom%surface%bnd%acc)   dom%surface%now%acc   = dom%vali%acc(dom%idx,day)
        if (dom%surface%bnd%smb)   dom%surface%now%smb   = dom%vali%massbal(dom%idx,day)
        if (dom%surface%bnd%smb)   dom%surface%now%smb_snow = dom%vali%massbal(dom%idx,day)
        if (dom%surface%bnd%lhf)   dom%surface%now%lhf   = dom%vali%lhf(dom%idx,day)
        if (dom%surface%bnd%shf)   dom%surface%now%shf   = dom%vali%shf(dom%idx,day)
        if (dom%surface%bnd%refr)  dom%surface%now%refr  = dom%vali%refr(dom%idx,day)
        !> 3. Call definitions for boundaries (surface_physics::surface_boundary_define)
        call surface_boundary_define(dom%surface%bnd,dom%surface%par%boundary)
        ! timing
        call cpu_time(start)
        !> 4. Call energy and mass balance subroutine (SEMIC) surface_physics::surface_energy_and_mass_balance
        call surface_energy_and_mass_balance(dom%surface%now,dom%surface%par,dom%surface%bnd,day,year)
        call cpu_time(finish)
        total_time = total_time + (finish-start)        
        dom%t = dom%t+1
        dom%tnow = dom%tnow + 1.0
        dom%driver%day_of_year = dom%driver%day_of_year + 1

        !> 5. Save current calculated variables for later use in state_class
        dom%state%tsurf(dom%idx,day)   =  dom%surface%now%tsurf
        dom%state%alb(dom%idx,day)     =  dom%surface%now%alb
        dom%state%melt(dom%idx,day)    =  dom%surface%now%melt
        dom%state%refr(dom%idx,day)    =  dom%surface%now%refr
        dom%state%acc(dom%idx,day)     =  dom%surface%now%acc
        dom%state%massbal(dom%idx,day) =  dom%surface%now%smb
        dom%state%swnet(dom%idx,day)   =  (1.0-dom%surface%now%alb)*dom%forc%swd(dom%idx,day)
        dom%state%shf(dom%idx,day)     =  dom%surface%now%shf
        dom%state%lhf(dom%idx,day)     =  dom%surface%now%lhf
        dom%state%hsnow(dom%idx,day)   =  dom%surface%now%hsnow
        dom%state%hice(dom%idx,day)    =  dom%surface%now%hice
        dom%state%acc(dom%idx,day)     =  dom%surface%now%acc
        dom%state%subl(dom%idx,day)    =  dom%surface%now%subl

        !> 6. Write output to file
        if ((year.eq.dom%nloop) .or. dom%driver%multi) then
            ! reset time counter and steps if output
            ! should be written only for the last year.
            if (.not.dom%driver%multi .and. day .eq. 1) then
                dom%tnow = 0.0
                dom%t = 1
                dom%driver%day_of_year = 1
            end if
            if (dom%driver%multi .and. day.eq.1) then
                dom%driver%day_of_year = 1
            end if
        end if

    end subroutine smb_update

    !> write output to NetCDF files
    subroutine smb_output(dom,start)
        type(smb_class), intent(in) :: dom !< domain object
        integer, intent(in) :: start !< first time step
        if (len_trim(dom%files%timser) > 0) call surface_write_ts(dom,dom%files%timser,start)
        if (len_trim(dom%files%daily) > 0) call surface_write(dom,dom%files%daily,start)
    end subroutine smb_output

    !> Clean-up: de-allocate mask, basin and surface state variables
    subroutine smb_end(dom)
        type(smb_class), intent(in out) :: dom !< domain object

        deallocate(dom%mask)
        deallocate(dom%basin)
        deallocate(dom%idx)
        !> call surface_physics::surface_dealloc
        call surface_dealloc(dom%surface%now)

    end subroutine smb_end

    !> Initialize file writing for 2D output variables.
    !! 1. Write x, y, and time dimensions
    !! 2. Write grid information
    subroutine surface_write_init(dom,fnm,time0,units)

        implicit none 

        type(smb_class), intent(in out)  :: dom !< domain object
        character(len=*), intent(in) :: fnm !< output file name
        character(len=*), intent(in) :: units !< time units
        integer, intent(in) :: time0 !< initial time step

        ! Initialize netcdf file and dimensions
        call nc_create(fnm)
        call nc_write_dim(fnm,"xc",  x=dom%grid%G%x,units="kilometers")
        call nc_write_dim(fnm,"yc",  x=dom%grid%G%y,units="kilometers")
        call nc_write_dim(fnm,"time", x=time0,dx=1,nx=1,units=trim(units))
        
        ! Write grid information
        call grid_write(dom%grid,fnm,xnm="xc",ynm="yc",create=.FALSE.)

        return
    end subroutine surface_write_init 

    !> Write 2D variables to file.
    !! \todo More general handling of output variables
    subroutine surface_write(dom,fnm,start)
        type(smb_class), intent(in) :: dom !< domain object
        character(len=*), intent(in)  :: fnm !< output file name
        integer, intent(in) :: start
        integer :: nt, t, first
        integer, allocatable :: a(:,:,:)

        allocate(a(dom%grid%G%nx,dom%grid%G%ny,size(dom%state%tsurf,2)))

        nt = size(dom%state%tsurf,2)
        first = (start-1)*nt+1
        write(*,"(2A)") "\x1B[32msemic_output:\x1B[0m Write 2D data to file ", trim(fnm)

        do t=first,first+nt-1
            call nc_write(fnm,"time",t-1,dim1="time",start=[t],count=[1])
        end do
        call nc_write(fnm,"tsurf",reshape(dom%state%tsurf,shape(a)),dim1="xc",dim2="yc",dim3="time",&
            units="K",start=[1,1,first],count=[dom%grid%G%nx,dom%grid%G%ny,nt])
        call nc_write(fnm,"alb",reshape(dom%state%alb,shape(a)),dim1="xc",dim2="yc",dim3="time",&
            units="1",start=[1,1,first],count=[dom%grid%G%nx,dom%grid%G%ny,nt])
        call nc_write(fnm,"hsnow",reshape(dom%state%hsnow,shape(a)),dim1="xc",dim2="yc",dim3="time",&
            units="m",start=[1,1,first],count=[dom%grid%G%nx,dom%grid%G%ny,nt])
        call nc_write(fnm,"hice",reshape(dom%state%hice,shape(a)),dim1="xc",dim2="yc",dim3="time",&
            units="m",start=[1,1,first],count=[dom%grid%G%nx,dom%grid%G%ny,nt])
        call nc_write(fnm,"acc",reshape(dom%state%acc,shape(a)),dim1="xc",dim2="yc",dim3="time",&
            units="m/s",start=[1,1,first],count=[dom%grid%G%nx,dom%grid%G%ny,nt])
        call nc_write(fnm,"melt",reshape(dom%state%melt,shape(a)),dim1="xc",dim2="yc",dim3="time",&
            units="m/s",start=[1,1,first],count=[dom%grid%G%nx,dom%grid%G%ny,nt])
        call nc_write(fnm,"smb",reshape(dom%state%massbal,shape(a)),dim1="xc",dim2="yc",dim3="time",&
            units="m/s",start=[1,1,first],count=[dom%grid%G%nx,dom%grid%G%ny,nt])
        call nc_write(fnm,"shf",reshape(dom%state%shf,shape(a)),dim1="xc",dim2="yc",dim3="time",&
            units="W/m2",start=[1,1,first],count=[dom%grid%G%nx,dom%grid%G%ny,nt])
        call nc_write(fnm,"lhf",reshape(dom%state%lhf,shape(a)),dim1="xc",dim2="yc",dim3="time",&
            units="W/m2",start=[1,1,first],count=[dom%grid%G%nx,dom%grid%G%ny,nt])
        call nc_write(fnm,"refr",reshape(dom%state%refr,shape(a)),dim1="xc",dim2="yc",dim3="time",&
            units="m/s",start=[1,1,first],count=[dom%grid%g%nx,dom%grid%g%ny,nt])
        call nc_write(fnm,"subl",reshape(dom%state%subl,shape(a)),dim1="xc",dim2="yc",dim3="time",&
            units="m/s",start=[1,1,first],count=[dom%grid%g%nx,dom%grid%g%ny,nt])
        call nc_write(fnm,"swnet",reshape(dom%state%swnet,shape(a)),&
            dim1="xc",dim2="yc",dim3="time",units="W/m2",start=[1,1,first],count=[dom%grid%G%nx,dom%grid%G%ny,nt])
        call nc_write(fnm,"swnet_mar",reshape((1.-dom%vali%alb)*dom%forc%swd,shape(a)),&
            dim1="xc",dim2="yc",dim3="time",units="W/m2",start=[1,1,first],count=[dom%grid%G%nx,dom%grid%G%ny,nt])

        deallocate(a)

    end subroutine


    !> Initialize file writing for time series of output variables.
    !! Creates time series file and add dimensions "time" and "basin"
    subroutine surface_write_ts_init(fnm,time0,units,bdim)

        implicit none 

        character(len=*), intent(in) :: fnm !< output file name
        character(len=*), intent(in) :: units !< time units
        integer, intent(in) :: time0 !< initial time step
        integer, intent(in) :: bdim  !< length of basin dimension
        integer :: i

        ! Initialize netcdf file and dimensions
        call nc_create(fnm)
        call nc_write_dim(fnm,"basin",x=[(i,i=1,bdim)],units="basin")
        call nc_write_dim(fnm,"time", x=time0,dx=1,nx=1,units=trim(units))

        return
    end subroutine surface_write_ts_init 

    !> Write time series of output variables to file.
    !! Uses utils::avg_over_mask
    !! \todo More general handling of output variables
    subroutine surface_write_ts(dom,fnm,start)
        type(smb_class), intent(in) :: dom
        character(len=*), intent(in)  :: fnm
        integer, intent(in) :: start
        integer :: bmax, i, first, t, nt
        double precision :: avg
        logical :: mask(size(dom%idx))

        nt = size(dom%state%tsurf,2)
        first = (start-1)*nt
        write(*,"(2A)") "\x1B[32msemic_output:\x1B[0m Write averaged time series to file ", trim(fnm)

        do t=first+1,first+nt
            call nc_write(fnm,"time",t-1,dim1="time",start=[t],count=[1])
        end do

        bmax = int(maxval(dom%basin))
        do i=1,bmax
            call nc_write(fnm,"basin",i,dim1="basin",start=[i],count=[1])
            where(int(dom%basin(dom%idx)).eq.i)
                mask = .true.
            elsewhere
                mask = .false.
            end where
            do t=1,nt
                ! model data
                call avg_over_mask(dom%state%tsurf(dom%idx,t),mask,avg)
                call nc_write(fnm,"tsurf_ts",avg,dim1="basin",dim2="time",units="K",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%state%hsnow(dom%idx,t),mask,avg)
                call nc_write(fnm,"hsnow_ts",avg,dim1="basin",dim2="time",units="m",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%state%refr(dom%idx,t),mask,avg)
                call nc_write(fnm,"refr_ts",avg,dim1="basin",dim2="time",units="m/s",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%state%acc(dom%idx,t),mask,avg)
                call nc_write(fnm,"acc_ts",avg,dim1="basin",dim2="time",units="m/s",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%state%melt(dom%idx,t),mask,avg)
                call nc_write(fnm,"melt_ts",avg,dim1="basin",dim2="time",units="m/s",start=[i,first+t],count=[1,1])
                call avg_over_mask( dom%state%massbal(dom%idx,t),mask,avg)
                call nc_write(fnm,"smb_ts",avg,dim1="basin",dim2="time",units="m/s",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%state%shf(dom%idx,t),mask,avg)
                call nc_write(fnm,"shf_ts",avg,dim1="basin",dim2="time",units="W/m2",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%state%lhf(dom%idx,t),mask,avg)
                call nc_write(fnm,"lhf_ts",avg,dim1="basin",dim2="time",units="W/m2",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%state%swnet(dom%idx,t),mask,avg)
                call nc_write(fnm,"swnet_ts",avg,dim1="basin",dim2="time",units="W/m2",start=[i,first+t],count=[1,1])
                ! validation data
                call avg_over_mask(dom%vali%tsurf(dom%idx,t),mask,avg)
                call nc_write(fnm,"tsurf_mar_ts",avg,dim1="basin",dim2="time",units="K",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%vali%hsnow(dom%idx,t),mask,avg)
                call nc_write(fnm,"hsnow_mar_ts",avg,dim1="basin",dim2="time",units="m",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%vali%refr(dom%idx,t),mask,avg)
                call nc_write(fnm,"refr_mar_ts",avg,dim1="basin",dim2="time",units="m/s",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%vali%acc(dom%idx,t),mask,avg)
                call nc_write(fnm,"acc_mar_ts",avg,dim1="basin",dim2="time",units="m/s",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%vali%melt(dom%idx,t),mask,avg)
                call nc_write(fnm,"melt_mar_ts",avg,dim1="basin",dim2="time",units="m/s",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%vali%massbal(dom%idx,t),mask,avg)
                call nc_write(fnm,"smb_mar_ts",avg,dim1="basin",dim2="time",units="m/s",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%vali%shf(dom%idx,t),mask,avg)
                call nc_write(fnm,"shf_mar_ts",avg,dim1="basin",dim2="time",units="W/m2",start=[i,first+t],count=[1,1])
                call avg_over_mask(dom%vali%lhf(dom%idx,t),mask,avg)
                call nc_write(fnm,"lhf_mar_ts",avg,dim1="basin",dim2="time",units="W/m2",start=[i,first+t],count=[1,1])
                call avg_over_mask((1.0-dom%vali%alb(dom%idx,t))*dom%forc%swd(dom%idx,t),mask,avg)
                call nc_write(fnm,"swnet_mar_ts",avg,dim1="basin",dim2="time",units="W/m2",start=[i,first+t],count=[1,1])
            end do
        end do

    end subroutine surface_write_ts


end module surface_driver

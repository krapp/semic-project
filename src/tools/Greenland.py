"""
:mod:`tools.Greenland` -- The Greenland Map
===========================================

"""
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt

class GreenlandMap(Basemap):
    """
    Draws a map of Greenland.
    """
    
    def outline(self,coast=True):
        """
        Draws coastlines, parallels, and meridians onto the map.
        """
        if coast: self.drawcoastlines()
        #self.drawparallels(np.arange(-80., 91., 5.), labels=[0, 1, 0, 0])
        #self.drawmeridians(np.arange(-180., 181., 15.), labels=[1, 0, 0, 1])
    
    def plotContour(self, data, colormap='jet', levels=None, title=''):
        self.outline()
        x, y = self(self.lon, self.lat)
        if levels is not None:
            im = self.contourf(x, y, data, levels, cmap=plt.cm.get_cmap(colormap,len(levels)-1))
        else:
		    im = self.pcolormesh(x, y, data, cmap=plt.cm.get_cmap(colormap))
        #self.colorbar(im, "bottom", size="5%", pad="5%",extend="both")
        plt.title(title)
	return im
        
    def plotColor(self, data, colormap='jet', levels=None, title=''):
        self.outline()
        x, y = self(self.lon, self.lat)
        if levels is not None:
            im = self.pcolor(x, y, data, levels, cmap=plt.cm.get_cmap(colormap,len(levels)-1))
        else:
            im = self.pcolor(x, y, data, cmap=plt.cm.get_cmap(colormap))
        self.colorbar(im, "bottom", size="5%", pad="5%")
        plt.title(title)


    def __init__(self, lon, lat, width=1575000, height=2850000, lat_0=72., lon_0=-41.5, *args, **kwargs):
        '''
        Constructor
        '''
        super(GreenlandMap, self).__init__(width=width, height=height, resolution='l', projection='stere', lat_0=lat_0, lon_0=lon_0)
        self.lon = lon
        self.lat = lat
        
def plotStations(stations):
    m = GreenlandMap(0,0)
    m.etopo()
    m.outline(coast=False)
    lats = []
    lons = []
    cities = []
    for i in stations:
        lats.append(i.lat)
        lons.append(i.lon)
        cities.append(i.name)
        # compute the native map projection coordinates for cities.
        x,y = m(lons,lats)
        # plot filled circles at the locations of the cities.
        m.plot(x,y,'ro')
        # plot the names of those five cities.
    for name,xpt,ypt in zip(cities,x,y):
        if name is 'JAR':
            plt.text(xpt-200000,ypt-90000,name)
        else:
            plt.text(xpt+50000,ypt+50000,name)
    plt.show() 
        


if __name__ == '__main__':
	m = GreenlandMap(0,0)
	m.outline()
	plt.show()

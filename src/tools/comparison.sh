#!/bin/bash

forcing_file=data/interim/forcing_${1}_am.nc
ice_mask=data/interim/ice_mask.nc
land_mask=data/interim/land_mask.nc
semic_file=data/processed/semic_${1}_opti_d_am.nc

cdo -s -setctomiss,0 -eqc,2 -selname,MSK $forcing_file $land_mask
cdo -s -setctomiss,0 -gtc,2.5 -selname,MSK $forcing_file $ice_mask

echo "Comparison for ${1}"

# surface temperature
echo "Difference in surface temperature SEMIC - MAR"
echo -n "over ice "
cdo -s --no_warnings -output -fldmean -add -sub -selname,tsurf $semic_file -addc,273.15 -selname,STT $forcing_file $ice_mask
echo -n "min"
cdo -s --no_warnings -output -fldmin -add -sub -selname,tsurf $semic_file -addc,273.15 -selname,STT $forcing_file $ice_mask
echo -n "max"
cdo -s --no_warnings -output -fldmax -add -sub -selname,tsurf $semic_file -addc,273.15 -selname,STT $forcing_file $ice_mask
echo -n "SEMIC   "
cdo -s --no_warnings -output -fldmean -add -selname,tsurf $semic_file $ice_mask
echo -n "MAR     "
cdo -s --no_warnings -output -fldmean -add -addc,273.15 -selname,STT $forcing_file $ice_mask

echo "----------"
echo -n "over land"
cdo -s --no_warnings -output -fldmean -add -sub -selname,tsurf $semic_file -addc,273.15 -selname,STT $forcing_file $land_mask
echo -n "min"
cdo -s --no_warnings -output -fldmin -add -sub -selname,tsurf $semic_file -addc,273.15 -selname,STT $forcing_file $land_mask
echo -n "max"
cdo -s --no_warnings -output -fldmax -add -sub -selname,tsurf $semic_file -addc,273.15 -selname,STT $forcing_file $land_mask
echo -n "SEMIC   "
cdo -s --no_warnings -output -fldmean -add -selname,tsurf $semic_file $land_mask
echo -n "MAR     "
cdo -s --no_warnings -output -fldmean -add -addc,273.15 -selname,STT $forcing_file $land_mask

echo ""

echo "Difference in surface mass balance SEMIC - MAR"
echo -n "over ice "
cdo -s --no_warnings -output -fldmean -add -sub -mulc,86.4e6 -selname,smb $semic_file -selname,SMB $forcing_file $ice_mask
echo -n "min"
cdo -s --no_warnings -output -fldmin -add -sub -mulc,86.4e6 -selname,smb $semic_file -selname,SMB $forcing_file $ice_mask
echo -n "max"
cdo -s --no_warnings -output -fldmax -add -sub -mulc,86.4e6 -selname,smb $semic_file -selname,SMB $forcing_file $ice_mask
echo -n "SEMIC   "
cdo -s --no_warnings -output -fldmean -add -mulc,86.4e6 -selname,smb $semic_file $ice_mask
echo -n "MAR     "
cdo -s --no_warnings -output -fldmean -add -selname,SMB $forcing_file $ice_mask

echo "----------"
echo -n "over land"
cdo -s --no_warnings -output -fldmean -add -sub -mulc,86.4e6 -selname,smb $semic_file -selname,SMB $forcing_file $land_mask
echo -n "min"
cdo -s --no_warnings -output -fldmin -add -sub -mulc,86.4e6 -selname,smb $semic_file -selname,SMB $forcing_file $land_mask
echo -n "max"
cdo -s --no_warnings -output -fldmax -add -sub -mulc,86.4e6 -selname,smb $semic_file -selname,SMB $forcing_file $land_mask
echo -n "SEMIC    "
cdo -s --no_warnings -output -fldmean -add -mulc,86.4e6 -selname,smb $semic_file $land_mask
echo -n "MAR       "
cdo -s --no_warnings -output -fldmean -add -selname,SMB $forcing_file $land_mask

echo ""

echo "Difference in surface melt SEMIC - MAR"
echo -n "over ice"
cdo -s --no_warnings -output -fldmean -add -sub -mulc,86.4e6 -selname,melt $semic_file -selname,ME $forcing_file $ice_mask
echo -n "min"
cdo -s --no_warnings -output -fldmin -add -sub -mulc,86.4e6 -selname,melt $semic_file -selname,ME $forcing_file $ice_mask
echo -n "max"
cdo -s --no_warnings -output -fldmax -add -sub -mulc,86.4e6 -selname,melt $semic_file -selname,ME $forcing_file $ice_mask
echo -n "SEMIC   "
cdo -s --no_warnings -output -fldmean -add -mulc,86.4e6 -selname,melt $semic_file $ice_mask
echo -n "MAR     "
cdo -s --no_warnings -output -fldmean -add -selname,ME $forcing_file $ice_mask

echo "----------"
echo -n "over land"
cdo -s --no_warnings -output -fldmean -add -sub -mulc,86.4e6 -selname,melt $semic_file -selname,ME $forcing_file $land_mask
echo -n "min"
cdo -s --no_warnings -output -fldmin -add -sub -mulc,86.4e6 -selname,melt $semic_file -selname,ME $forcing_file $land_mask
echo -n "max"
cdo -s --no_warnings -output -fldmax -add -sub -mulc,86.4e6 -selname,melt $semic_file -selname,ME $forcing_file $land_mask
echo -n "SEMIC   "
cdo -s --no_warnings -output -fldmean -add -mulc,86.4e6 -selname,melt $semic_file $land_mask
echo -n "MAR     "
cdo -s --no_warnings -output -fldmean -add -selname,ME $forcing_file $land_mask

echo ""

echo "Difference in net shortwave radiation SEMIC - MAR"
echo -n "over ice  "
cdo -s --no_warnings -output -fldmean -add -sub -selname,swnet $semic_file -selname,swnet_mar $semic_file $ice_mask
echo -n "min"
cdo -s --no_warnings -output -fldmin -add -sub -selname,swnet $semic_file -selname,swnet_mar $semic_file $ice_mask
echo -n "max"
cdo -s --no_warnings -output -fldmax -add -sub -selname,swnet $semic_file -selname,swnet_mar $semic_file $ice_mask
echo -n "SEMIC    "
cdo -s --no_warnings -output -fldmean -add -selname,swnet $semic_file $ice_mask
echo -n "MAR     "
cdo -s --no_warnings -output -fldmean -add -selname,swnet_mar $semic_file $ice_mask

echo "----------"
echo -n "over land"
cdo -s --no_warnings -output -fldmean -add -sub -selname,swnet $semic_file -selname,swnet_mar $semic_file $land_mask
echo -n "min"
cdo -s --no_warnings -output -fldmin -add -sub -selname,swnet $semic_file -selname,swnet_mar $semic_file $land_mask
echo -n "max"
cdo -s --no_warnings -output -fldmax -add -sub -selname,swnet $semic_file -selname,swnet_mar $semic_file $land_mask
echo -n "SEMIC    "
cdo -s --no_warnings -output -fldmean -add -selname,swnet $semic_file $land_mask
echo -n "MAR      "
cdo -s --no_warnings -output -fldmean -add -selname,swnet_mar $semic_file $land_mask

.PHONY: clean semic

coord_ver = 0.1
semic_ver = 1.2
ncio_ver  = 1.1

FC            = gfortran
MPIFC         = mpif90
NETCDF_FFLAGS = -I$(shell nc-config --includedir)
NETCDF_FLIBS  = -L$(shell nc-config --libdir)
LDFLAGS       = -L./external/coordinates-$(coord_ver) -lcoordinates -lnetcdff -lnetcdf
INCLUDES      = $(NETCDF_FFLAGS) -I./external/coordinates-$(coord_ver)/.obj
NCFLAGS       = $(NETCDF_FLIBS)
FLAGS         = -J$(OBJ_DIR)

SRC_DIR     = .
OBJ_DIR     = ./.obj

semic_bin     = semic.x
particles_bin = run_particles.x


ifeq ($(debug),1)
        FFLAGS   = -Og -p -fcheck=all -fbackslash -Wall -Wextra -fbounds-check -fbacktrace -cpp -ffpe-trap=zero,overflow,underflow
else
        FFLAGS   = -O3 -fbackslash -cpp
endif

# ifort options
ifeq ($(ifort),1)
        FC = ifort
        MPIFC = mpiifort
        FLAGS = -module $(OBJ_DIR)
        FFLAGS = -O3 -xHost -ipo -assume bscc -fpp -DUSE_INTEL
        ifeq ($(debug), 1)
                FFLAGS  = -C -traceback -ftrapuv -fpe0 -assume bscc -check all -fpp -DUSE_INTEL
        endif
endif

# List all the sources
SRCS = external/ncio-$(ncio_ver)/ncio.f90 \
       models/semic_project/interpolation2.f90 \
       models/semic_project/utils.f90 \
       external/semic-$(semic_ver)/surface_physics.f90 \
       models/semic_project/surface_driver_utilities.f90 \
       models/semic_project/surface_driver.f90

# Define the rule to make object file from f90
$(OBJ_DIR)/%.o : $(SRC_DIR)/%.f90
	$(MPIFC) $(FFLAGS) -c $< -o $@ -I$(OBJ_DIR) $(FLAGS) $(INCLUDES)

$(semic_bin) : $(SRCS:%.f90=$(OBJ_DIR)/%.o) $(OBJ_DIR)/models/semic_project/main.o
	$(FC) $(FFLAGS) $(INCLUDES) -o ../bin/$@ $^ $(LDFLAGS) $(NCFLAGS)

$(particles_bin): $(SRCS:%.f90=$(OBJ_DIR)/%.o) $(OBJ_DIR)/models/semic_project/run_particles.o
	$(MPIFC) $(FFLAGS) $(INCLUDES) -o ../bin/$@ $^ $(LDFLAGS) $(NCFLAGS)

semic : make_dirs externals $(semic_bin) $(particles_bin)

make_dirs :
	mkdir -p $(OBJ_DIR)/models/semic_project
	mkdir -p $(OBJ_DIR)/external/ncio-$(ncio_ver)
	mkdir -p $(OBJ_DIR)/external/semic-$(semic_ver)
	mkdir -p $(SRC_DIR)/external

externals: $(SRC_DIR)/external/ncio-$(ncio_ver) $(SRC_DIR)/external/coordinates-$(coord_ver) $(SRC_DIR)/external/semic-$(semic_ver)
	cd $(SRC_DIR)/external/coordinates-$(coord_ver) && ifort=$(ifort) debug=$(debug) make coord-static

$(SRC_DIR)/external/semic-$(semic_ver):
	wget -qO- https://github.com/mkrapp/semic/archive/v$(semic_ver).tar.gz | tar xz -C $(SRC_DIR)/external

$(SRC_DIR)/external/ncio-$(ncio_ver):
	wget -qO- https://github.com/alex-robinson/ncio/archive/v$(ncio_ver).tar.gz | tar xz -C $(SRC_DIR)/external

$(SRC_DIR)/external/coordinates-$(coord_ver):
	wget -qO- https://github.com/alex-robinson/coordinates/archive/v$(coord_ver).tar.gz | tar xz -C $(SRC_DIR)/external
	patch $(SRC_DIR)/external/coordinates-$(coord_ver)/Makefile $(SRC_DIR)/coord.patch

install-optimization: $(SRC_DIR)/external/semic-$(semic_ver)
	cd external/semic-1.0/optimize && make python-install

clean:
	rm -rf *.mod *.so *.o $(OBJ_DIR) ./external/coord/libcoordinates.a
	$(MAKE) -C $(SRC_DIR)/external/coordinates-$(coord_ver) clean
	$(MAKE) -C $(SRC_DIR)/external/ncio-$(ncio_ver) clean
